import { Point } from './point';
import { Drawing } from './drawing';

export class Shape extends Drawing{
    shapeId:number;
    shapeTypeId:number;
    radiusX:number;
    radiusY:number;
    centerPointX:number;
    centerPointY:number;
    // isActive:boolean;
}

