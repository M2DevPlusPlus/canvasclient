export class Drawing {
   drawingId:number;
   pictureId:number;
   lineColor:string;
   fillColor:string;
   comment:string;
   userId:number; 
   userName:string;
}

