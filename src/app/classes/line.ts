import { Point } from './point';
import { Drawing } from './drawing';

export class Line extends Drawing {
    lineId:number;
    startPointX:number;
    startPointY:number;
    endPointX:number;
    endPointY:number;
    // isActive:boolean;
}
