export class Picture {
    pictureId:number;
    userId:number;
    pictureUrl:string;
    pictureName:string;
    userTypeId:number;
    descriptions:string;
    // isActive?:boolean;
}
