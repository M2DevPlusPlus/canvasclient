export class PicturesForUser {
    pictureUserId:number;
    userId:number;
    userTypeId:number;
    pictureId:number;
    isActive:boolean;
}
