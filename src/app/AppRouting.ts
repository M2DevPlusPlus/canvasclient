import { LoginComponent } from './components/login/login.component';
import { Canvas2Component } from './components/canvas2/canvas2.component';
import { componentFactoryName } from '@angular/compiler';
import { NewPictureComponent } from './components/new-picture/new-picture.component';
import { PictureListComponent } from './components/picture-list/picture-list.component';

export const appRouting = [
    {
        path: 'Login',
        component: LoginComponent

    },
    {
        path: 'EditPicture',
        component: Canvas2Component
    },
    {
        path: 'NewPicture',
        component: NewPictureComponent
    },
    {
        path: 'PictureList',
        component: PictureListComponent
    }


]