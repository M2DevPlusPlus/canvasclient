import { Component, OnInit } from '@angular/core';
import { PictureService } from 'src/app/services/picture.service';
import { UsersService } from 'src/app/services/users.service';
import { Picture } from 'src/app/classes/picture';
import { Router } from '@angular/router';

@Component({
  selector: 'app-picture-list',
  templateUrl: './picture-list.component.html',
  styleUrls: ['./picture-list.component.css']
})
export class PictureListComponent implements OnInit {
  isLoad: boolean;

  constructor(public picServ: PictureService,public userServ: UsersService,public location:Router) { }
picList:Picture[]=[];
  ngOnInit() {

    this.picServ.getPictureList(this.userServ.currentUser.userId).subscribe(
      data=>{
        console.log;
        
        debugger
      this.picList=data ;
      this.isLoad=true;
      }
    );
  }
  editPicture(pic:Picture){
    debugger
    this.picServ.currentPicture=pic
    this.location.navigate(['/EditPicture'])
  }
  NewPicture(){
    this.location.navigate(['/NewPicture']);
  }

}
