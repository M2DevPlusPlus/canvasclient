import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { PictureService } from 'src/app/services/picture.service';
import { Router } from '@angular/router';
import { Picture } from 'src/app/classes/picture';

@Component({
  selector: 'app-new-picture',
  templateUrl: './new-picture.component.html',
  styleUrls: ['./new-picture.component.css']
})
export class NewPictureComponent implements OnInit {

  constructor(public pictureService: PictureService,public UserService:UsersService, public location: Router) { }
  fileToUpload: File;
pictureName:string;
  ngOnInit() {
  }
  hundleFileInput(files: FileList) {
    debugger;
    this.pictureName=files.item(0).name;
    console.log(this.pictureName);
    this.fileToUpload = files.item(0);
  }
  uploudFileToActivity() {
       this.pictureService.postFile(this.fileToUpload).subscribe(
      (data: any) => {
        this.pictureService.imageUrl = data;
        this.pictureService.currentPicture=new Picture();
        this.pictureService.currentPicture.pictureName=this.pictureName;
        this.pictureService.currentPicture.pictureUrl=this.pictureService.imageUrl;
        this.pictureService.currentPicture.descriptions="";
        this.pictureService.currentPicture.userTypeId=1;
        this.pictureService.currentPicture.userId=this.UserService.currentUser.userId

        console.log(data);
      }
    )

  }
  savePicture() {
    this.pictureService.savePicture().subscribe(
      (data: any) => {
        this.pictureService.currentPicture.pictureId = data;console.log(data);debugger;
         this.location.navigate(['/EditPicture']); console.log(data);
      });
        

  }
}
