import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { fromEvent, Subject, Observable, Subscription } from 'rxjs';
import { takeUntil, mergeMap, map, pairwise, switchMap, buffer, switchMapTo, filter } from 'rxjs/operators';
import { UsersService } from 'src/app/services/users.service';
import { Point } from '../../classes/point';
import { Shape } from 'src/app/classes/shape';
import { Drawing } from 'src/app/classes/drawing';
import { Line } from 'src/app/classes/line';
import { PictureService } from 'src/app/services/picture.service';
import { ShapeService } from 'src/app/services/shape.service';
import { LineService } from 'src/app/services/line.service';

class MovingData {
  /**
   *
   */
  constructor(
    public shape: Shape,
    public distX: number,
    public DistY: number,
    public point: Point

  ) {

  }
}


@Component({
  selector: 'app-canvas2',
  templateUrl: './canvas2.component.html',
  styleUrls: ['./canvas2.component.css']
})
export class Canvas2Component implements AfterViewInit, OnInit {

  @ViewChild('can1', { static: true }) CanvasEl: ElementRef;
  @ViewChild('can2', { static: true }) CanvasEl2: ElementRef;
  cx: CanvasRenderingContext2D;
  cx2: CanvasRenderingContext2D;
  polyline: Subject<Point>;
  mover: Subject<MovingData>;
  // mover:Subject<[Shape,number,number]>
  shape = 1;
  canvas1: HTMLCanvasElement;
  img = '../../assets/IMG_2506.JPG';
  rect: any;
  shapes: Shape[] = [];
  lines: Line[] = [];
  // tslint:disable-next-line:variable-name
  prev_shapes: Shape[] = [];
  // tslint:disable-next-line:variable-name
  prev_lines: Line[] = [];
  mousedown1: Subscription;
  mousedown2: Subscription;
  mousedownDraw: Subscription;
  mousedownSelect: Subscription;
  poly: Subscription;
  moveSub: Subscription;

  // tslint:disable-next-line:max-line-length
  constructor(public UserService: UsersService, public pictureService: PictureService, public shapeService: ShapeService, public lineService: LineService) {
  }
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.polyline = new Subject<Point>();
    this.mover = new Subject<any>();
    this.canvas1 = this.CanvasEl.nativeElement;
    const canvas2: HTMLCanvasElement = this.CanvasEl2.nativeElement;
    this.cx = this.canvas1.getContext('2d');
    this.cx2 = canvas2.getContext('2d');
    this.cx.lineWidth = 3;
    this.cx2.lineWidth = 3;
    this.rect = this.canvas1.getBoundingClientRect();
    this.mousedown1 = fromEvent(this.canvas1, 'mousedown').subscribe();
    this.mousedown2 = fromEvent(this.canvas1, 'mousedown').subscribe();
    // this.listenMouse(this.canvas1);
    // this.listenMouse2(this.canvas1);
    this.ToShape(1);
  }
  ToSelect() {
    this.shape = 0;
    this.listenMouse2(this.canvas1);
    this.mousedownDraw.unsubscribe();
    this.poly.unsubscribe();
  }
  ToShape(t: number) {
    this.shape = t;
    this.listenMouse(this.canvas1);
    this.mousedownSelect.unsubscribe();
    this.moveSub.unsubscribe();
  }

  listenMouse(canvas: HTMLCanvasElement) {
    const mousedown$ = fromEvent(canvas, 'mousedown');
    const mouseup$ = fromEvent(canvas, 'mouseup');
    const mousemove$ = fromEvent(canvas, 'mousemove');
    const mouseleave$ = fromEvent(canvas, 'mouseleave');
    this.rect = canvas.getBoundingClientRect();
    // previous and current position with the offset
    this.poly = this.polyline.pipe(
      buffer(mouseup$)
    ).subscribe(shape => {
      this.DrawShape(shape);
    });
    this.mousedownDraw = mousedown$.pipe(
      switchMap(_ => {
        return mousemove$.pipe(
          map((e: any) => (
            {
              x: e.clientX - this.rect.left,
              y: e.clientY - this.rect.top
            })),
          // complete inner observable on mouseup event
          takeUntil(mouseup$),
          takeUntil(mouseleave$),
          pairwise()
        );
      }
      ))
      .subscribe(
        (res: [Point, Point]) => {

          if (this.shape > 0) {
            this.polyline.next(res[1]);
            this.DrawPoint(res[0], res[1]);
          } else { }
        });
  }


  listenMouse2(canvas: HTMLCanvasElement) {
    const mousedown$ = fromEvent(canvas, 'mousedown');
    const mouseup$ = fromEvent(canvas, 'mouseup');
    const mousemove$ = fromEvent(canvas, 'mousemove');
    const mouseleave$ = fromEvent(canvas, 'mouseleave');
    this.rect = canvas.getBoundingClientRect();

    this.moveSub = this.mover.pipe(
      buffer(mouseup$)
    ).subscribe((obj: MovingData[]) => {
      if (obj && obj.length > 0) {
        this.realMove(obj[obj.length - 1]);
      }
    });

    this.mousedownSelect = mousedown$.pipe(
      map((e: MouseEvent) =>
        this.findShape(e)
      ),
      filter( x=> x != null),
      switchMap(obj => {
        return mousemove$.pipe(
          map((a: MouseEvent) =>
            new MovingData(obj.shape, obj.distX, obj.DistY, new Point(a.clientX - this.rect.left, a.clientY - this.rect.top))
          ),
          takeUntil(mouseup$),
          takeUntil(mouseleave$),
        );
      })
    ).
      subscribe((obj: MovingData) => {
        this.mover.next(obj);
        this.moveShape(obj);
      }
      );
  }

  DrawPoint(p: Point, p1: Point) {
    this.cx.beginPath();
    this.cx.moveTo(p.x, p.y);
    this.cx.lineTo(p1.x, p1.y);
    this.cx.stroke();
  }

  GetCenterPoint(points: Point[]) {
    let sumx = 0;
    let sumy = 0;
    points.forEach(p => { sumx += p.x; sumy += p.y; });
    const avgx = sumx / points.length;
    const avgy = sumy / points.length;
    return new Point(avgx, avgy);
  }

  GetDistance(p1: Point, p2: Point) {
    return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
  }

  GetRadius(centerPoint: Point, points: Point[]) {
    let sum = 0;
    points.forEach(p => sum += this.GetDistance(centerPoint, p));
    return sum / points.length;
  }

  avg(listNum: Array<number>) {
    let sum = 0;
    let count = 0;
    listNum.forEach(a => { sum += a; count++; });
    return sum / count;
  }

  func1(points: Point[], center: Point) {
    const sumx1 = 0;
    const countx1 = 0;
    let rx: number;
    let ry: number;
    const preX = this.avg(points.filter(x => x.x < center.x).map(val => center.x - val.x));
    const postX = this.avg(points.filter(x => x.x > center.x).map(val => val.x - center.x));
    rx = (postX + preX) / 2;
    const preY = this.avg(points.filter(y => y.y < center.y).map(val => center.y - val.y));
    const postY = this.avg(points.filter(y => y.y > center.y).map(val => val.y - center.y));
    ry = (postY + preY) / 2;
    const a = [rx, ry];
    return a;
  }
  DrawShape(points: Point[]) {
    this.cx.clearRect(0, 0, this.rect.width, this.rect.height);
    const center = this.GetCenterPoint(points);
    const radius = this.GetRadius(center, points);
    this.cx2.beginPath();
    const r = this.func1(points, center);
    const dr = new Drawing();
    switch (this.shape) {
      case 1:
        this.cx2.rect(center.x - r[0], center.y - r[1], r[0] * 2, r[1] * 2);
        const s = new Shape();
        console.log(center.x, center.y);
        s.radiusX = r[0]; s.radiusY = r[1]; s.centerPointX = center.x; s.centerPointY = center.y;
        s.pictureId = this.pictureService.currentPicture.pictureId;
        s.userName = this.UserService.currentUser.userName; s.userId = this.UserService.currentUser.userId;
        s.comment = ''; s.lineColor = '';
        s.fillColor = ''; s.shapeTypeId = 1;
        this.shapes.push(s);
        break;
      case 2:
        this.cx2.ellipse(center.x, center.y, r[0], r[1], 0, 0, 2 * Math.PI);
        const s1 = new Shape();
        console.log(center.x, center.y);
        s1.radiusX = r[0]; s1.radiusY = r[1]; s1.centerPointX = center.x; s1.centerPointY = center.y;
        s1.pictureId = this.pictureService.currentPicture.pictureId;
        s1.userName = this.UserService.currentUser.userName; s1.userId = this.UserService.currentUser.userId;
        s1.comment = ''; s1.lineColor = '';
        s1.fillColor = ''; s1.shapeTypeId = 2;
        this.shapes.push(s1);
        break;
      case 3:
        if (points.length !== 0) {
          this.cx2.moveTo(points[0].x, points[0].y);
          this.cx2.lineTo(points[points.length - 1].x, points[points.length - 1].y);
          const l = new Line();
          l.comment = ''; l.fillColor = ''; l.lineColor = '';
          l.pictureId = this.pictureService.currentPicture.pictureId;
          l.userId = this.UserService.currentUser.userId; l.userName = this.UserService.currentUser.userName;
          l.startPointX = points[0].x; l.startPointY = points[0].y;
          l.endPointX = points[points.length - 1].x; l.endPointY = points[points.length - 1].y;
          this.lines.push(l);
        }
        break;
      default:
        break;
    }
    this.cx2.stroke();
  }

  SaveChanges() {
    console.log(this.shapes);
    console.log(this.lines);
    this.shapeService.addRangeShapes(this.shapes)
      .subscribe(
        (data: any) => {
          console.log('yy' + data);
        });
    this.lineService.addRangeLines(this.lines)
      .subscribe(
        (data: any) => {
          console.log('yy' + data);
        });
  }

  GetDrawings() {
    this.lineService.getLinesByPictureId(this.pictureService.currentPicture.pictureId)
      .subscribe(
        (data: any) => {
          this.lineService.linesList = data;
          console.log('yy' + data[0].pictureId);
          this.shapeService.getShapesByPictureId(this.pictureService.currentPicture.pictureId)
            .subscribe(
              (data: any) => {
                this.shapeService.shapesList = data;
                console.log('shapes  ' + data[0].pictureId);
                this.DrawDrawings();
              });
        });
  }

  DrawDrawings() {
    // debugger;
    this.cx2.beginPath();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.shapeService.shapesList.length; i++) {
      switch (this.shapeService.shapesList[i].shapeTypeId) {
        case 1:
          this.cx2.rect(this.shapeService.shapesList[i].centerPointX - this.shapeService.shapesList[i].radiusX,
            this.shapeService.shapesList[i].centerPointY - this.shapeService.shapesList[i].radiusY,
            this.shapeService.shapesList[i].radiusX * 2, this.shapeService.shapesList[i].radiusY * 2);
          break;
        case 2:
          this.cx2.ellipse(this.shapeService.shapesList[i].centerPointX, this.shapeService.shapesList[i].centerPointY,
            this.shapeService.shapesList[i].radiusX, this.shapeService.shapesList[i].radiusY, 0, 0, 2 * Math.PI);
          break;
      }
    }
    this.cx2.stroke();
    this.cx2.beginPath();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.lineService.linesList.length; i++) {
      this.cx2.moveTo(this.lineService.linesList[i].startPointX, this.lineService.linesList[i].startPointY);
      this.cx2.lineTo(this.lineService.linesList[i].endPointX, this.lineService.linesList[i].endPointY);
    }
    this.cx2.stroke();

  }

  GetDrawing() {
    this.lineService.getLinesByPictureId(this.pictureService.currentPicture.pictureId)
      .subscribe(
        (data: any) => {
          this.lineService.linesList = data;
          console.log('yy' + data);
        });
  }

  findShape(e: MouseEvent) {
    debugger
    if (this.shape === 0) {
      let shapeToMove: Shape;
      let index: number;
      let distX: number;
      let distY: number;
      const p = new Point(e.clientX - this.rect.left, e.clientY - this.rect.top);
      for (index = 0; index < this.shapeService.shapesList.length; index++) {
        const element = this.shapeService.shapesList[index];
        const dist = this.GetDistance(new Point(element.centerPointX, element.centerPointY), p);
        if (dist < element.radiusX && dist < element.radiusY) {
          shapeToMove = { ...element };
          console.log(element);
          break;
        }
      }
      if (shapeToMove) {
        this.shapeService.shapesList.splice(index, 1);
        this.cx2.clearRect(0, 0, this.rect.width, this.rect.height);
        this.DrawDrawings();
        this.cx.strokeStyle = 'red';
        this.DrawFullShape(shapeToMove, this.cx);
        this.cx.strokeStyle = 'red';
        distX = shapeToMove.centerPointX - p.x;
        distY = shapeToMove.centerPointY - p.y;
        console.log('mouse down!!!!!!!!!!!!!!!!!!');
      }
      const m = new MovingData(shapeToMove, distX, distY, p);
      if (m.shape !== undefined && m.shape !== null) {
        return m;
      }
      debugger
      return null;
    }
  }
  moveShape(obj: MovingData) {
    // var p = new Point(e.clientX - this.rect.left, e.clientY - this.rect.top);
    // console.log(p);
    this.cx.clearRect(0, 0, this.rect.width, this.rect.height);
    const s = new Shape();
    s.centerPointX = obj.point.x + obj.distX;
    s.centerPointY = obj.point.y + obj.DistY;
    s.radiusX = obj.shape.radiusX;
    s.radiusY = obj.shape.radiusY;
    s.shapeTypeId = obj.shape.shapeTypeId;
    // console.log('s',s);
    // console.log('obj.shape',obj.shape);
    this.cx.strokeStyle = 'red';
    this.DrawFullShape(s, this.cx);
  }
  realMove(obj: MovingData) {
    // if (this.Grabbing) {
    // var p = new Point(e.clientX - this.rect.left, e.clientY - this.rect.top);
    console.log('mouse up!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    this.cx.clearRect(0, 0, this.rect.width, this.rect.height);
    const s = { ...obj.shape };
    s.centerPointX = obj.point.x + obj.distX;
    s.centerPointY = obj.point.y + obj.DistY;

    this.shapes.push(s);
    this.shapeService.shapesList.push(s);
    this.cx.strokeStyle = 'black';
    this.cx2.strokeStyle = 'black';
    // console.log('s',s);
    // console.log('obj.shape',obj.shape);
    this.DrawFullShape(s, this.cx2);

  }

  DrawFullShape(s: Shape, cx: CanvasRenderingContext2D) {
    cx.beginPath();
    switch (s.shapeTypeId) {

      case 1:
        console.log(s);
        cx.rect(s.centerPointX - s.radiusX,
          s.centerPointY - s.radiusY,
          s.radiusX * 2, s.radiusY * 2);
        cx.stroke();
        break;
      case 2:
        this.cx2.ellipse(s.centerPointX, s.centerPointY,
          s.radiusX, s.radiusY, 0, 0, 2 * Math.PI);
        cx.stroke();

        break;
      default:
        break;

    }
  }
}



