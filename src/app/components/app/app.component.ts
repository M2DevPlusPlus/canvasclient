import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { UsersService } from 'src/app/services/users.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements AfterViewInit {
  IsLogin = false;
  ngAfterViewInit(): void {
    this.Login();
  }
  /**
   *
   */
  constructor(private location: Router, public userService: UsersService) {

  }
  Login() {
    this.location.navigate(['/Login']);
  }
  EditPicture() {
    this.location.navigate(['/EditPicture']);
  }

}