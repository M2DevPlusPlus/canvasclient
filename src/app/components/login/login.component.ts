import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/classes/user';
// import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;


  constructor(public usersService:UsersService ,public location:Router) { }
  
  ngOnInit() {
    this.user=new User();
    this.user.userId=1;
    this.user.emailAddress="";
  }

  login(){
    this.usersService.GetUser(this.user).subscribe(
      data => {this.usersService.currentUser = data;
      console.log(data);
      alert("ברוך הבא ל"+this.usersService.currentUser.userName);
      debugger
        this.location.navigate(['/PictureList'])
      }
    );

  }
  
}
