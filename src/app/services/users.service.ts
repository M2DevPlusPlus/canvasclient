import { Injectable } from '@angular/core';
import { User } from '../classes/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  BASIC_URL = "https://localhost:44330/api/User/"
  constructor(private http: HttpClient) { }
  currentUser: User;

  GetUser(user: User) {
    debugger
    console.log(user);
    
    return this.http.post<User>(this.BASIC_URL + "GetUser", user);
     
  }
  AddNewUser(user: User) {
    debugger
    return this.http.post<User>(this.BASIC_URL + "AddNewUser", user).subscribe(
      data => { this.currentUser = data; debugger; alert(this.currentUser.emailAddress) }
    );
  }

}