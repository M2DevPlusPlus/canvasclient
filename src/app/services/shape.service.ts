import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Shape } from '../classes/shape';

@Injectable({
  providedIn: 'root'
})
export class ShapeService {
  BASIC_URL = "https://localhost:44330/api/Shape/";
  shapesList=new Array<Shape>(); 
  constructor(private http: HttpClient) { }
  addRangeShapes(shapes:Shape[])
  {
    return this.http.post(this.BASIC_URL + "addRangeShapes",shapes);
   }
   getShapesByPictureId(pictureId:number)
  {
    return this.http.get(this.BASIC_URL + "getShapesByPictureId/"+pictureId);
  }
}
