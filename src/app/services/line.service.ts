import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Line } from '../classes/line';

@Injectable({
  providedIn: 'root'
})

export class LineService {
  BASIC_URL = "https://localhost:44330/api/Line/"; 
  linesList=new Array<Line>();
  constructor(private http: HttpClient) { }
  addRangeLines(lines:Line[])
  {
    return this.http.post(this.BASIC_URL + "addRangeLines",lines);
  }
  getLinesByPictureId(pictureId:number)
  {
    debugger
    return this.http.get(this.BASIC_URL + "getLineByPictureId/"+pictureId);
  }
}
