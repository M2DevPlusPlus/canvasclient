import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Picture } from '../classes/picture';
import { Shape } from '../classes/shape';

@Injectable({
  providedIn: 'root'
})
export class PictureService {
 
  BASIC_URL = "https://localhost:44330/api/Picture/"; 
  imageUrl: string;
  currentPicture:Picture;
  constructor(private http: HttpClient) { }
  public postFile(image: File) {
    const formData = new FormData();
    formData.append('image', image);
    return this.http.post(this.BASIC_URL + "UploadFiles", formData);
    alert("hhh");
  }
savePicture(){
return this.http.post(this.BASIC_URL + "AddPicture",this.currentPicture);
}
 getPictureList(userId: number) {
  return this.http.get<Picture[]>(this.BASIC_URL + "GetPicturesPerUser/"+userId);
}
}
