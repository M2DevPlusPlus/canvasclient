import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { AppComponent } from './components/app/app.component';
import { Canvas2Component } from './components/canvas2/canvas2.component';
import { UsersService } from './services/users.service';
import { HttpClientModule } from '@angular/common/http';
import { NewPictureComponent } from './components/new-picture/new-picture.component';
import { appRouting } from './AppRouting';
import { RouterModule } from '@angular/router';
import { PictureService } from './services/picture.service';
import { PictureListComponent } from './components/picture-list/picture-list.component';
import { LineService } from './services/line.service';
import { ShapeService } from './services/shape.service';

@NgModule({
  declarations: [
    AppComponent,
    Canvas2Component,
    LoginComponent,
    NewPictureComponent,
    PictureListComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpClientModule,RouterModule.forRoot(appRouting,{enableTracing: true})
  ],
  providers: [UsersService,PictureService,LineService,ShapeService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
